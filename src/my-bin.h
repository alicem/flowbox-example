#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define MY_TYPE_BIN (my_bin_get_type())

G_DECLARE_FINAL_TYPE (MyBin, my_bin, MY, BIN, GtkBin)

G_END_DECLS
